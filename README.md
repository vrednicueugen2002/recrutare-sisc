# Enigma 2
Ei bine, aici nu este nimic stricat, dar asta nu înseamnă că toate lucrurile sunt la locul lor. Departamentul IT a întâmpinat câteva probleme în realizarea formularului pentru înregistrarea studenților. Acestui formular îi lipsesc validările, ceea ce înseamnă că avem nevoie de voi să ne ajutați să-l securizăm. Așadar...

**1.Aplicați formularului următoarele validări**

a) toate câmpurile pe care le introduceți din formularul de pe FRONT vor fi non-empty;

b) câmpuri cu limită de caractere: nume, prenume (minim 3 caractere, maxim 30);

c) câmpul nume va permite doar litere, iar primul caracter trebuie să fie neapărat Uppercase(adică literede forma A,B,C...Z, nu a,b,c...z);

d) câmpul prenume trebuie să accepte doar litere și spații (se pot introduceți ambele prenume în același câmp, de exemplu: **Adi Matei**)

e) validare pentru email (să accepte conturi de @stud.ase.ro, @gmail.com și @yahoo.com);

f) validare pentru data început de semestru și sfârșit de semestru (formă specifică dd/mm/yyyy);

g) validare pentru data de început să fie mai mică decât data de sfârșit;

h) verificați dacă ziua curentă este între datele de început și sfârșit;

i) validare pentru numărul de telefon ( lungime = 10, doar numere);

j) validare pentru specializare, să accepte doar litere și spații(de exemplu: Informatică Economică)

k) validare pentru linkul de facebook, să accepte formatul "www.faceboook.com"

l) validare pentru legitimația de student,să aibă mereu S in față + un cod de 6 numere (**de exemplu S116215**)

**Done?**

![enter image description here](https://media.giphy.com/media/xUNda34dU83brWxbdS/giphy.gif)

Acum că suntem "protejați", să îl **dezvoltăm** puțin:

**2. Creați o nouă bază de date cu un nume reprezentativ** (nu uitati sa modificați si în server.js). De asemenea, **schimbați și portul**.

**3.Adăugați câmpul CNP în formular.**
 - Acesta trebuie să existe atât în partea de front-end, cât și în baza de date, cu tipul VARCHAR, de 20 de caractere.
 -   Aplicați acestuia următoarele validări: non-empty, doar cifre, lungime = 13;
 
 ![enter image description here](https://media.giphy.com/media/xTeV7HxRwc7mZxBIBi/giphy.gif)

**4. Adăugați câmpul vârstă în formular.**
 -   Acesta trebuie să existe atât în partea de front-end, cât și în baza de date, cu tipul VARCHAR, de 3 caractere.
 -   Aplicați acestuia următoarele validări: **non-empty, doar cifre, lungime minimă un caracter, lungime maximă 3 caractere.**
 -   Verificati daca varsta corespunde cu cea care reiese din CNP;
  
 ![enter image description here](https://media.giphy.com/media/eCqFYAVjjDksg/giphy.gif)
 
**5. Asigurați-vă de unicitatea câmpului email în baza de date.**

![enter image description here](https://media.giphy.com/media/dBrZhsBDQ4X6w/giphy.gif)

**6. Adăugați câmpul "medie" în formular.**
- validari: să sa fie un numar intre 0 și 10, NOT NULL și cu două zecimale.

**7. Adaugați câmpul "forma_finantare" în formular.**

> **Nota**: Trebuie să apară doar în baza de date și să fie creat pe baza mediei. Astfel, dacă media este mai mare decât 7, forma_finantare o să fie 'buget', dacă nu, o să fie 'taxa'.

**8.Adăugați câmpul "gen" în formular.**
 - Acesta trebuie să apară doar în baza de date și să fie creat pe baza CNP-ului.
> **Notă**: Dacă prima cifră a CNP-ului este **1, 3 sau 5** în baza de date se va introduce litera "**M**", iar dacă prima cifră a CNP-ului este **2, 4 sau 6** în baza de date se va introduce litera "**F**"
Dacă ai ajuns până aici cu bine, atunci **FELICITĂRI...**

![enter image description here](https://media.giphy.com/media/NiBMdMizycNCE/giphy.gif)

Un SiSCot cuminte iși predă codul la timp, așa că:
**Upload your homework on the [SiSCovery App.](https://siscovery.sisc.ro)**


