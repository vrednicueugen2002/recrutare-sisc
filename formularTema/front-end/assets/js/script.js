$(document).ready(function () {
    $('select').material_select();
});

document.querySelector('#regbutton').addEventListener("click", (e) => {
    e.preventDefault();
    toastr.remove()
    const dateStudent = {
        nume: document.querySelector('#nume').value,
        prenume: document.querySelector('#prenume').value,
        telefon: document.querySelector('#telefon').value,
        email: document.querySelector('#email').value,
        specializare: document.querySelector('#specializare').value,
        facebook: document.querySelector('#facebook').value,
        cod_legitimatie: document.querySelector('#cod_legitimatie').value,
        data_inceput_semestru: document.querySelector('#data_inceput_semestru').value,
        data_sfarsit_semestru: document.querySelector('#data_sfarsit_semestru').value,
        cnp: document.querySelector('#cnp').value,
        varsta: document.querySelector('#varsta').value,
        medie: document.querySelector('#medie').value
    }
    axios.post('/student', dateStudent)
        .then((response) => {
            toastr.success("Student adaugat cu succes!");
        })
        .catch((error) => {
            const values = Object.values(error.response.data)
            console.log(error);
            values.map(item => {
                toastr.error(item)
            })
        })

}, false)