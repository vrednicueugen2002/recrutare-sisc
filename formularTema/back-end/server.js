const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 8081;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "concurs_polihack",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS studenti_inscrisi(nume VARCHAR(255), prenume  VARCHAR(255), telefon  VARCHAR(255) , cnp VARCHAR(20) , gen VARCHAR(1), email  VARCHAR(255) UNIQUE , specializare VARCHAR(255),facebook VARCHAR(255),cod_legitimatie VARCHAR(255),data_inceput_semestru VARCHAR(10),data_sfarsit_semestru VARCHAR(10) , varsta VARCHAR(3) , medie FLOAT, forma_finantare VARCHAR(10))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/student", (req, res) => {
  let student = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    specializare: req.body.specializare,
    facebook: req.body.facebook,
    cod_legitimatie: req.body.cod_legitimatie,
    data_inceput_semestru: req.body.data_inceput_semestru,
    data_sfarsit_semestru: req.body.data_sfarsit_semestru,
    varsta: req.body.varsta,
    medie: req.body.medie
  };

  var forma_finantare;
  var gen;
  let error = [];
  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ ok pa

  if (!student.nume || !student.prenume || !student.telefon || !student.email || !student.specializare || !student.facebook || !student.cod_legitimatie || !student.data_inceput_semestru || !student.data_sfarsit_semestru || !student.cnp || !student.varsta || !student.medie) {
    error.push('Unul sau mai multe campuri nu au fost completate');
    console.log('Unul sau mai multe campuri nu au fost completate');
  } else {
    if (student.nume.length < 3 || student.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!student.nume.match("^[A-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    } else if (!student.nume.charAt(0).match("^[A-Z]+$")) {
      console.log('Numele trebuie sa inceapa cu o majuscula!');
      error.push('Numele trebuie sa inceapa cu o majuscula!');
    }

    if (student.prenume < 3 || student.nume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!student.prenume.match(/\s/) && !student.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }

    if (!student.email.includes("@gmail.com") && !student.email.includes("@yahoo.com") && !student.email.includes("@stud.ase.ro")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }

    if (student.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!student.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }

    function validateUSDate(strValue) {
      var objRegExp = /^\d{1,2}(|\/|)\d{1,2}\1\d{4}$/;

      //check to see if in correct format
      if (!objRegExp.test(strValue))
        return false; //doesn't match pattern, bad date
      return true;
    }

    if (!validateUSDate(student.data_inceput_semestru)) {
      console.log("Data inceput nu are formatul corect.")
      error.push("Data inceput nu are formatul corect");
    }

    if (!validateUSDate(student.data_sfarsit_semestru)) {
      console.log("Data sfarsit nu are formatul corect.");
      error.push("Data sfarsit nu are formatul corect");
    }

    if (validateUSDate(student.data_inceput_semestru) && validateUSDate(student.data_sfarsit_semestru)) {
      //functia new.date() citeste o data dintr-un string sub formatul mm/dd/yyyy 
      var parts = student.data_inceput_semestru.split('/');
      var dataInitiala = new Date(parts[2], parts[1] - 1, parts[0], 2, 0, 0);
      var parts2 = student.data_sfarsit_semestru.split('/');
      var dataFinala = new Date(parts2[2], parts2[1] - 1, parts2[0], 2, 0, 0);

      if (dataFinala <= dataInitiala) {
        console.log("Data finala e mai mica decat data initiala!");
        error.push("Data finala e mai mica decat data initiala!");
      } else {

        var dataCurenta = new Date();

        if (dataCurenta <= dataInitiala || dataCurenta >= dataFinala) {
          console.log("Ziua curenta nu este intre datele de inceput si sfarsit!");
          error.push("Ziua curenta nu este intre datele de inceput si sfarsit!");
        }
      }

    }

    if (!student.specializare.match(/\s/) && !student.specializare.match("^[A-Za-z]+$")) {
      console.log("Specializarea trebuie sa contina doar litere si spatii!");
      error.push("Specializarea trebuie sa contina doar litere si spatii");
    }

    if (!student.facebook.includes("www.facebook.com")) {
      console.log("Link facebook invalid!");
      error.push("Link facebook invalid!");
    }

    if (student.cod_legitimatie.charAt(0) === 'S' && student.cod_legitimatie.length == 7) {
      for (let i = 1; i < 7; i++) {
        if (student.cod_legitimatie.charAt(i) < '0' || student.cod_legitimatie.charAt(i) > '9') {
          console.log("Codul legitimatiei este invalid!");
          error.push("Codul legitimatiei este invalid!");
          break;
        }
      }
    } else {
      console.log("Codul legitimatiei este invalid!");
      error.push("Codul legitimatiei este invalid!");
    }

    if (student.cnp.length != 13) {
      console.log("CNP-ul trebuie sa fie de 13 cifre!");
      error.push("CNP-ul trebuie sa fie de 13 cifre!");
    } else if (!student.cnp.match("^[0-9]+$")) {
      console.log("CNP-ul trebuie sa contina doar cifre!");
      error.push("CNP-ul trebuie sa contina doar cifre!");
    } else if (student.cnp.charAt(0) == '9') {
      console.log("CNP invalid!");
      error.push("CNP invalid!");
    } else {
      if (student.cnp[0] == '1' || student.cnp[0] == '3' || student.cnp[0] == '5')
        gen = 'M';
      else
        gen = 'F';
    }

    if (student.varsta.length < 1 || student.varsta.length > 3) {
      console.log("Varsta invalida!");
      error.push("Varsta invalida!");
    } else
      if (!student.varsta.match("^[0-9]+$")) {
        console.log("Varsta invalida!");
        error.push("Varsta invalida!");
      }


    function validareVarsta(cnp, varsta) {

      // daca de exemplu cnp-ul e sub forma x02...... anul nasterii poate fi ori 1902 ori 2002, ori 1802, depinde de prima cifra
      // 1 pentru persoanele de sex masculin născute între anii 1900 - 1999
      // 2 pentru persoanele de sex feminin născute între anii 1900 - 1999
      // 3 pentru persoanele de sex masculin născute între anii 1800 - 1899
      // 4 pentru persoanele de sex feminin născute între anii 1800 - 1899
      // 5 pentru persoanele de sex masculin născute între anii 2000 - 2099
      // 6 pentru persoanele de sex feminin născute între anii 2000 - 2099
      // 7 pentru persoanele rezidente, de sex masculin
      // 8 pentru persoanele rezidente, de sex feminin
      // la 7 si 8, prima cifra a CNP-ului spune doar ca sunt persoanele rezidente, nu si anul nasterii asa ca voi considera 2000+ anul nasterii

      var data_nastere = '';
      if (cnp.charAt(0) == '1' || cnp.charAt(0) == '2')
        data_nastere = "19";
      else if (cnp.charAt(0) == '3' || cnp.charAt(0) == '4')
        data_nastere = "18";
      else if (cnp.charAt(0) == '5' || cnp.charAt(0) == '6' || cnp.charAt(0) == '7' || cnp.charAt(0) == '8')
        data_nastere = "20";

      function data_nastere_completa(an, cnp) {
        for (var i = 1; i < 7; i++) {
          if (i == 3 || i == 5)
            an = an + '/';
          an = an + cnp.charAt(i);
        }
        return an;
      }

      data_nastere = data_nastere_completa(data_nastere, cnp);

      function calculateAge(birthday) { // birthday is a date
        var ageDifMs = Date.now() - new Date(birthday);
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
      }

      var varstaObtinuta = calculateAge(data_nastere);

      if (parseInt(varsta) != varstaObtinuta) {
        console.log("Varsta nu corespunde cu cea din CNP");
        error.push("Varsta nu corespunde cu cea din CNP");
      }
    }

    validareVarsta(student.cnp, student.varsta);

    var media = parseFloat(student.medie);

    if (media < 0 || media > 10 || (media != media.toFixed(2))) {
      console.log("Media introdusa este incorecta!");
      error.push("Media introdusa este incorecta!");
    } else {
      if (media > 7)
        forma_finantare = 'buget';
      else
        forma_finantare = 'taxa';
    }

  }

  if (error.length === 0) {
    const sql = `INSERT INTO studenti_inscrisi (nume,
      prenume,
      telefon,
      cnp,
      gen,
      email,
      specializare,
      facebook,
      cod_legitimatie,
      data_inceput_semestru,
      data_sfarsit_semestru,
      varsta, 
      medie,
      forma_finantare) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;

    connection.query(
      sql,
      [
        student.nume,
        student.prenume,
        student.telefon,
        student.cnp,
        gen,
        student.email,
        student.specializare,
        student.facebook,
        student.cod_legitimatie,
        student.data_inceput_semestru,
        student.data_sfarsit_semestru,
        student.varsta,
        student.medie,
        forma_finantare
      ],
      function (err, result) {
        if (err) {
          error.push("Exista deja e-mailul in baza de date!");
          console.log("Exista deja e-mailul in baza de date!");
          res.status(500).send(error);
          return;
        };
        console.log("Student creat cu succes!");
        res.status(200).send({
          message: "Student creat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Studentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html
